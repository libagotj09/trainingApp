# Copyright (c) 2013, Frappe Technologies and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
    return get_columns(), get_data(filters)

def Merge(dict1, dict2):
    return (dict1.update(dict2))

def get_data(filters):
    print(f"\n\n\n{filters}\n\n\n\n")

    conditions = " 1=1 "
    if(filters.get('first_name')):conditions += f"AND first_name='{filters.get('first_name')}'"
    if(filters.get('middle_name')):conditions += f"AND middle_name='{filters.get('middle_name')}'"
    if(filters.get('last_name')):conditions += f"AND last_name='{filters.get('last_name')}'"
    if(filters.get('sport')):conditions += f"AND sports='{filters.get('sport')}'"
    
    x = 0
    data1 = frappe.db.sql(f"""SELECT first_name, middle_name, last_name, full_name FROM `tabAthlete` WHERE{conditions};""", as_dict=True)
    
    for i in data1:
        data2 = frappe.db.sql(f"""SELECT sport, start_date, end_date, no_of_games FROM `tabGames` WHERE parent = '{i['full_name']}';""", as_dict=True)
        data1[x] = Merge(i, data2[0])
        x = x + 1

    data3 = list()
    dict_data = dict()

    for data in data1:
        dict_data['Full Name'] = data['full_name'] #dict_data paramaters are based on the .js file while the data parameter is based on how we get data from the sql in a form of dictionaries
        dict_data['full_name'] = data['full_name']

        dict_data['First Name'] = data['first_name']
        dict_data['first_name'] = data['first_name']

        dict_data['Middle Name'] = data['middle_name']
        dict_data['middle_name'] = data['middle_name']

        dict_data['Last Name'] = data['last_name']
        dict_data['last_name'] = data['last_name']

        dict_data['Sports'] = data['sport'] 
        dict_data['sports'] = data['sport']

        dict_data['Start Date'] = data['start_date']
        dict_data['start_date'] = data['start_date']

        dict_data['End Date'] = data['end_date']
        dict_data['end_date'] = data['end_date']

        dict_data['No. of Games'] = data['no_of_games']
        dict_data['no_of_games'] = data['no_of_games']
        data3.append(dict_data)
        dict_data = {}

    return data3


def get_columns():
    return[   
        "ID:Link/Athlete:200",
        "First Name:Data:120",
        "Middle Name:Data:100",
        "Last Name:Data:100",
        "Sports:Data:100",
        "Start Date:Date:100",
        "End Date:Date:100",
        "No. of Games:Int:120"
    ]
