# -*- coding: utf-8 -*-
# Copyright (c) 2021, TJ and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Student(Document):
    def on_submit(self):
        doc = frappe.get_doc({
            'doctype': 'Patient', 
            'school_id': self.school_id,
            'age': self.age,
            'birthday': self.birthday,
            'section': self.section
        })

        if frappe.db.exists('Patient', doc.name):
            print("Already exists")
        else:
            doc.insert()

    def on_cancel(self):
        doc = frappe.get_list('Patient', filters={'school_id':self.school_id})
        frappe.db.sql(f"""DELETE FROM tabPatient WHERE tabPatient.name = '{doc[0].name}';""")

    def on_trash(self):
        doc = frappe.get_list('Patient', filters={'school_id':self.school_id})
        frappe.db.sql(f"""DELETE FROM tabPatient WHERE tabPatient.name = '{doc[0].name}';""")
